// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFActionTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ACTIONSSYSTEM_ACFActionTypes_generated_h
#error "ACFActionTypes.generated.h already included, missing '#pragma once' in ACFActionTypes.h"
#endif
#define ACTIONSSYSTEM_ACFActionTypes_generated_h

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFWarpInfo_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FACFWarpInfo>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_93_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFWarpReproductionInfo_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FACFWarpReproductionInfo>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_118_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFMontageInfo_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FACFMontageInfo>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_159_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionState_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FActionState>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_177_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionsSet_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FActionsSet>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_187_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBoneSections_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FBoneSections>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_200_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionsArray_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FActionsArray>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_218_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionConfig_Statics; \
	ACTIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ACTIONSSYSTEM_API UScriptStruct* StaticStruct<struct FActionConfig>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_SPARSE_DATA
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFAttackTypes(); \
	friend struct Z_Construct_UClass_UACFAttackTypes_Statics; \
public: \
	DECLARE_CLASS(UACFAttackTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ActionsSystem"), NO_API) \
	DECLARE_SERIALIZER(UACFAttackTypes)


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFAttackTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFAttackTypes(UACFAttackTypes&&); \
	NO_API UACFAttackTypes(const UACFAttackTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFAttackTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFAttackTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFAttackTypes) \
	NO_API virtual ~UACFAttackTypes();


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_281_PROLOG
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_SPARSE_DATA \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_INCLASS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h_283_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ACTIONSSYSTEM_API UClass* StaticClass<class UACFAttackTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_ActionsSystem_Public_ACFActionTypes_h


#define FOREACH_ENUM_EMONTAGEREPRODUCTIONTYPE(op) \
	op(EMontageReproductionType::ERootMotion) \
	op(EMontageReproductionType::ERootMotionScaled) \
	op(EMontageReproductionType::EMotionWarped) \
	op(EMontageReproductionType::ECurveOverrideSpeed) \
	op(EMontageReproductionType::ECurveSnapsOnTarget) \
	op(EMontageReproductionType::ECurveOverrideSpeedAndDirection) 

enum class EMontageReproductionType : uint8;
template<> struct TIsUEnumClass<EMontageReproductionType> { enum { Value = true }; };
template<> ACTIONSSYSTEM_API UEnum* StaticEnum<EMontageReproductionType>();

#define FOREACH_ENUM_EWARPTARGETTYPE(op) \
	op(EWarpTargetType::ETargetTransform) \
	op(EWarpTargetType::ETargetComponent) 

enum class EWarpTargetType : uint8;
template<> struct TIsUEnumClass<EWarpTargetType> { enum { Value = true }; };
template<> ACTIONSSYSTEM_API UEnum* StaticEnum<EWarpTargetType>();

#define FOREACH_ENUM_EACTIONPRIORITY(op) \
	op(EActionPriority::ENone) \
	op(EActionPriority::ELow) \
	op(EActionPriority::EMedium) \
	op(EActionPriority::EHigh) \
	op(EActionPriority::EHighest) 

enum class EActionPriority : uint8;
template<> struct TIsUEnumClass<EActionPriority> { enum { Value = true }; };
template<> ACTIONSSYSTEM_API UEnum* StaticEnum<EActionPriority>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
