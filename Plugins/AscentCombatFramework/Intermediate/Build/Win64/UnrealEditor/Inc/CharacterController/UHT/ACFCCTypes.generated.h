// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFCCTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHARACTERCONTROLLER_ACFCCTypes_generated_h
#error "ACFCCTypes.generated.h already included, missing '#pragma once' in ACFCCTypes.h"
#endif
#define CHARACTERCONTROLLER_ACFCCTypes_generated_h

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_41_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAimOffset_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FAimOffset>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_68_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharacterGroundInfo_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FCharacterGroundInfo>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_95_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovStances_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FMovStances>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_132_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOverlayConfig_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FOverlayConfig>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_153_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFLocomotionState_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FACFLocomotionState>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_223_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FReplicatedAcceleration_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FReplicatedAcceleration>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_239_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSnapConfiguration_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct();


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FSnapConfiguration>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_SPARSE_DATA
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFCCTypes(); \
	friend struct Z_Construct_UClass_UACFCCTypes_Statics; \
public: \
	DECLARE_CLASS(UACFCCTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CharacterController"), NO_API) \
	DECLARE_SERIALIZER(UACFCCTypes)


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFCCTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFCCTypes(UACFCCTypes&&); \
	NO_API UACFCCTypes(const UACFCCTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFCCTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFCCTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFCCTypes) \
	NO_API virtual ~UACFCCTypes();


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_260_PROLOG
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_SPARSE_DATA \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_INCLASS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h_262_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CHARACTERCONTROLLER_API UClass* StaticClass<class UACFCCTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_ACFCCTypes_h


#define FOREACH_ENUM_EROTATIONMODE(op) \
	op(ERotationMode::EForwardFacing) \
	op(ERotationMode::EStrafing) 

enum class ERotationMode : uint8;
template<> struct TIsUEnumClass<ERotationMode> { enum { Value = true }; };
template<> CHARACTERCONTROLLER_API UEnum* StaticEnum<ERotationMode>();

#define FOREACH_ENUM_ELOCOMOTIONSTATE(op) \
	op(ELocomotionState::EIdle) \
	op(ELocomotionState::EWalk) \
	op(ELocomotionState::EJog) \
	op(ELocomotionState::ESprint) 

enum class ELocomotionState : uint8;
template<> struct TIsUEnumClass<ELocomotionState> { enum { Value = true }; };
template<> CHARACTERCONTROLLER_API UEnum* StaticEnum<ELocomotionState>();

#define FOREACH_ENUM_EMOVEMENTSTANCE(op) \
	op(EMovementStance::EIdle) \
	op(EMovementStance::EAiming) \
	op(EMovementStance::EBlock) \
	op(EMovementStance::ECustom) 

enum class EMovementStance : uint8;
template<> struct TIsUEnumClass<EMovementStance> { enum { Value = true }; };
template<> CHARACTERCONTROLLER_API UEnum* StaticEnum<EMovementStance>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
