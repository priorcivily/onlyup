// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BehavioralThree/ACFUpdatePatrolBTService.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeACFUpdatePatrolBTService() {}
// Cross Module References
	AIFRAMEWORK_API UClass* Z_Construct_UClass_UACFUpdatePatrolBTService();
	AIFRAMEWORK_API UClass* Z_Construct_UClass_UACFUpdatePatrolBTService_NoRegister();
	AIMODULE_API UClass* Z_Construct_UClass_UBTService();
	UPackage* Z_Construct_UPackage__Script_AIFramework();
// End Cross Module References
	void UACFUpdatePatrolBTService::StaticRegisterNativesUACFUpdatePatrolBTService()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UACFUpdatePatrolBTService);
	UClass* Z_Construct_UClass_UACFUpdatePatrolBTService_NoRegister()
	{
		return UACFUpdatePatrolBTService::StaticClass();
	}
	struct Z_Construct_UClass_UACFUpdatePatrolBTService_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTService,
		(UObject* (*)())Z_Construct_UPackage__Script_AIFramework,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BehavioralThree/ACFUpdatePatrolBTService.h" },
		{ "ModuleRelativePath", "Public/BehavioralThree/ACFUpdatePatrolBTService.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UACFUpdatePatrolBTService>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::ClassParams = {
		&UACFUpdatePatrolBTService::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::Class_MetaDataParams), Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UACFUpdatePatrolBTService()
	{
		if (!Z_Registration_Info_UClass_UACFUpdatePatrolBTService.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UACFUpdatePatrolBTService.OuterSingleton, Z_Construct_UClass_UACFUpdatePatrolBTService_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UACFUpdatePatrolBTService.OuterSingleton;
	}
	template<> AIFRAMEWORK_API UClass* StaticClass<UACFUpdatePatrolBTService>()
	{
		return UACFUpdatePatrolBTService::StaticClass();
	}
	UACFUpdatePatrolBTService::UACFUpdatePatrolBTService(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UACFUpdatePatrolBTService);
	UACFUpdatePatrolBTService::~UACFUpdatePatrolBTService() {}
	struct Z_CompiledInDeferFile_FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_BehavioralThree_ACFUpdatePatrolBTService_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_BehavioralThree_ACFUpdatePatrolBTService_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UACFUpdatePatrolBTService, UACFUpdatePatrolBTService::StaticClass, TEXT("UACFUpdatePatrolBTService"), &Z_Registration_Info_UClass_UACFUpdatePatrolBTService, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UACFUpdatePatrolBTService), 832577206U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_BehavioralThree_ACFUpdatePatrolBTService_h_3008656533(TEXT("/Script/AIFramework"),
		Z_CompiledInDeferFile_FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_BehavioralThree_ACFUpdatePatrolBTService_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_BehavioralThree_ACFUpdatePatrolBTService_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
