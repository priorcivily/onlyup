// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAscentEditorExtensions_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_AscentEditorExtensions;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_AscentEditorExtensions()
	{
		if (!Z_Registration_Info_UPackage__Script_AscentEditorExtensions.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/AscentEditorExtensions",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000040,
				0x6C63D954,
				0xA37CC98E,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_AscentEditorExtensions.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_AscentEditorExtensions.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_AscentEditorExtensions(Z_Construct_UPackage__Script_AscentEditorExtensions, TEXT("/Script/AscentEditorExtensions"), Z_Registration_Info_UPackage__Script_AscentEditorExtensions, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x6C63D954, 0xA37CC98E));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
