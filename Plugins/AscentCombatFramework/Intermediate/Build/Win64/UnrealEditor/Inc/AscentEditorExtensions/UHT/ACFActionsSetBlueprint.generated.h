// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFActionsSetBlueprint.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASCENTEDITOREXTENSIONS_ACFActionsSetBlueprint_generated_h
#error "ACFActionsSetBlueprint.generated.h already included, missing '#pragma once' in ACFActionsSetBlueprint.h"
#endif
#define ASCENTEDITOREXTENSIONS_ACFActionsSetBlueprint_generated_h

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_SPARSE_DATA
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFActionsSetBlueprint(); \
	friend struct Z_Construct_UClass_UACFActionsSetBlueprint_Statics; \
public: \
	DECLARE_CLASS(UACFActionsSetBlueprint, UBlueprint, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AscentEditorExtensions"), NO_API) \
	DECLARE_SERIALIZER(UACFActionsSetBlueprint)


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFActionsSetBlueprint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFActionsSetBlueprint(UACFActionsSetBlueprint&&); \
	NO_API UACFActionsSetBlueprint(const UACFActionsSetBlueprint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFActionsSetBlueprint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFActionsSetBlueprint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFActionsSetBlueprint) \
	NO_API virtual ~UACFActionsSetBlueprint();


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_12_PROLOG
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_SPARSE_DATA \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_INCLASS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASCENTEDITOREXTENSIONS_API UClass* StaticClass<class UACFActionsSetBlueprint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Public_ACFActionsSetBlueprint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
