// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVehicleSystem_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_VehicleSystem;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_VehicleSystem()
	{
		if (!Z_Registration_Info_UPackage__Script_VehicleSystem.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/VehicleSystem",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xC08500A5,
				0x42EEC361,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_VehicleSystem.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_VehicleSystem.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_VehicleSystem(Z_Construct_UPackage__Script_VehicleSystem, TEXT("/Script/VehicleSystem"), Z_Registration_Info_UPackage__Script_VehicleSystem, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xC08500A5, 0x42EEC361));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
