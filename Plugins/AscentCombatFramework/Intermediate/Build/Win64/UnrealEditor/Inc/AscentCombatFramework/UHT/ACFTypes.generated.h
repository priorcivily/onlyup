// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Game/ACFTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASCENTCOMBATFRAMEWORK_ACFTypes_generated_h
#error "ACFTypes.generated.h already included, missing '#pragma once' in ACFTypes.h"
#endif
#define ASCENTCOMBATFRAMEWORK_ACFTypes_generated_h

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDamageInfluence_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FDamageInfluence>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDamageInfluences_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FDamageInfluences>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_79_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRagdollImpulse_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FRagdollImpulse>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_118_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImpactEffect_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FImpactEffect>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_154_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionChances_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FActionChances>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_180_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionsChances_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FActionsChances>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_189_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOnHitActionChances_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FActionChances Super;


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FOnHitActionChances>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_198_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEffectByDamageType_Statics; \
	ASCENTCOMBATFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> ASCENTCOMBATFRAMEWORK_API UScriptStruct* StaticStruct<struct FEffectByDamageType>();

#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_SPARSE_DATA
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_ACCESSORS
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFTypes(); \
	friend struct Z_Construct_UClass_UACFTypes_Statics; \
public: \
	DECLARE_CLASS(UACFTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AscentCombatFramework"), NO_API) \
	DECLARE_SERIALIZER(UACFTypes)


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFTypes(UACFTypes&&); \
	NO_API UACFTypes(const UACFTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFTypes) \
	NO_API virtual ~UACFTypes();


#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_218_PROLOG
#define FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_SPARSE_DATA \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_ACCESSORS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_INCLASS_NO_PURE_DECLS \
	FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h_220_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASCENTCOMBATFRAMEWORK_API UClass* StaticClass<class UACFTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_build_U5M_Marketplace_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Game_ACFTypes_h


#define FOREACH_ENUM_EDEATHTYPE(op) \
	op(EDeathType::EGoRagdoll) \
	op(EDeathType::EDeathAction) \
	op(EDeathType::EDoNothing) 

enum class EDeathType : uint8;
template<> struct TIsUEnumClass<EDeathType> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EDeathType>();

#define FOREACH_ENUM_EDAMAGEACTIVATIONTYPE(op) \
	op(EDamageActivationType::ERight) \
	op(EDamageActivationType::ELeft) \
	op(EDamageActivationType::EBoth) \
	op(EDamageActivationType::EPhysical) 

enum class EDamageActivationType : uint8;
template<> struct TIsUEnumClass<EDamageActivationType> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EDamageActivationType>();

#define FOREACH_ENUM_EBATTLESTATE(op) \
	op(EBattleState::EExploration) \
	op(EBattleState::EBattle) 

enum class EBattleState : uint8;
template<> struct TIsUEnumClass<EBattleState> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EBattleState>();

#define FOREACH_ENUM_EDAMAGEZONE(op) \
	op(EDamageZone::ENormal) \
	op(EDamageZone::EImmune) \
	op(EDamageZone::EHighDefense) \
	op(EDamageZone::EHighDamage) 

enum class EDamageZone : uint8;
template<> struct TIsUEnumClass<EDamageZone> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EDamageZone>();

#define FOREACH_ENUM_EHITDIRECTION(op) \
	op(EHitDirection::EFront) \
	op(EHitDirection::EFrontLeft) \
	op(EHitDirection::EFrontRight) \
	op(EHitDirection::ELeftLeg) \
	op(EHitDirection::ERightLeg) \
	op(EHitDirection::EHead) \
	op(EHitDirection::ERight) \
	op(EHitDirection::ELeft) \
	op(EHitDirection::EBack) 

enum class EHitDirection : uint8;
template<> struct TIsUEnumClass<EHitDirection> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EHitDirection>();

#define FOREACH_ENUM_EACTIONDIRECTION(op) \
	op(EActionDirection::Front) \
	op(EActionDirection::Back) \
	op(EActionDirection::Left) \
	op(EActionDirection::Right) \
	op(EActionDirection::EveryDirection) 

enum class EActionDirection : uint8;
template<> struct TIsUEnumClass<EActionDirection> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<EActionDirection>();

#define FOREACH_ENUM_ECOMBATTYPE(op) \
	op(ECombatType::EUnarmed) \
	op(ECombatType::EMelee) \
	op(ECombatType::ERanged) 

enum class ECombatType : uint8;
template<> struct TIsUEnumClass<ECombatType> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<ECombatType>();

#define FOREACH_ENUM_ECOMBATBEHAVIORTYPE(op) \
	op(ECombatBehaviorType::EMelee) \
	op(ECombatBehaviorType::ERanged) 

enum class ECombatBehaviorType : uint8;
template<> struct TIsUEnumClass<ECombatBehaviorType> { enum { Value = true }; };
template<> ASCENTCOMBATFRAMEWORK_API UEnum* StaticEnum<ECombatBehaviorType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
