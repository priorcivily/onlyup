// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpellActions_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_SpellActions;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_SpellActions()
	{
		if (!Z_Registration_Info_UPackage__Script_SpellActions.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/SpellActions",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xAB20A390,
				0x7421AF57,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_SpellActions.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_SpellActions.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_SpellActions(Z_Construct_UPackage__Script_SpellActions, TEXT("/Script/SpellActions"), Z_Registration_Info_UPackage__Script_SpellActions, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xAB20A390, 0x7421AF57));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
