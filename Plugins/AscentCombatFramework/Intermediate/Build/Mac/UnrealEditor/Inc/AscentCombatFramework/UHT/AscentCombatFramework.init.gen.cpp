// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAscentCombatFramework_init() {}
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnActorDetected__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnBattleStateChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnCharacterDeath__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnCharacterFullyInitialized__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnCombatTypeChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnCounterAttackTriggered__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnCrouchStateChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnDamageInflicted__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnDamageReceived__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnDeathEvent__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnDefenseStanceChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnInteractableRegistered__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnMovesetChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnPossessedCharacterChanged__DelegateSignature();
	ASCENTCOMBATFRAMEWORK_API UFunction* Z_Construct_UDelegateFunction_AscentCombatFramework_OnRagdollStateChanged__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_AscentCombatFramework;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_AscentCombatFramework()
	{
		if (!Z_Registration_Info_UPackage__Script_AscentCombatFramework.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnActorDetected__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnBattleStateChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnCharacterDeath__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnCharacterFullyInitialized__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnCombatTypeChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnCounterAttackTriggered__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnCrouchStateChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnDamageInflicted__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnDamageReceived__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnDeathEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnDefenseStanceChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnInteractableRegistered__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnMovesetChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnPossessedCharacterChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_AscentCombatFramework_OnRagdollStateChanged__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/AscentCombatFramework",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x1B2847D9,
				0x42E31284,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_AscentCombatFramework.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_AscentCombatFramework.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_AscentCombatFramework(Z_Construct_UPackage__Script_AscentCombatFramework, TEXT("/Script/AscentCombatFramework"), Z_Registration_Info_UPackage__Script_AscentCombatFramework, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x1B2847D9, 0x42E31284));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
