// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AscentCombatFramework/Public/Animation/ACFOverrideSpeedNotifyState.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeACFOverrideSpeedNotifyState() {}
// Cross Module References
	ASCENTCOMBATFRAMEWORK_API UClass* Z_Construct_UClass_UACFOverrideSpeedNotifyState();
	ASCENTCOMBATFRAMEWORK_API UClass* Z_Construct_UClass_UACFOverrideSpeedNotifyState_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAnimNotifyState();
	UPackage* Z_Construct_UPackage__Script_AscentCombatFramework();
// End Cross Module References
	void UACFOverrideSpeedNotifyState::StaticRegisterNativesUACFOverrideSpeedNotifyState()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UACFOverrideSpeedNotifyState);
	UClass* Z_Construct_UClass_UACFOverrideSpeedNotifyState_NoRegister()
	{
		return UACFOverrideSpeedNotifyState::StaticClass();
	}
	struct Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimNotifyState,
		(UObject* (*)())Z_Construct_UPackage__Script_AscentCombatFramework,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Animation/ACFOverrideSpeedNotifyState.h" },
		{ "ModuleRelativePath", "Public/Animation/ACFOverrideSpeedNotifyState.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UACFOverrideSpeedNotifyState>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::ClassParams = {
		&UACFOverrideSpeedNotifyState::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001130A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::Class_MetaDataParams), Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UACFOverrideSpeedNotifyState()
	{
		if (!Z_Registration_Info_UClass_UACFOverrideSpeedNotifyState.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UACFOverrideSpeedNotifyState.OuterSingleton, Z_Construct_UClass_UACFOverrideSpeedNotifyState_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UACFOverrideSpeedNotifyState.OuterSingleton;
	}
	template<> ASCENTCOMBATFRAMEWORK_API UClass* StaticClass<UACFOverrideSpeedNotifyState>()
	{
		return UACFOverrideSpeedNotifyState::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UACFOverrideSpeedNotifyState);
	UACFOverrideSpeedNotifyState::~UACFOverrideSpeedNotifyState() {}
	struct Z_CompiledInDeferFile_FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Animation_ACFOverrideSpeedNotifyState_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Animation_ACFOverrideSpeedNotifyState_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UACFOverrideSpeedNotifyState, UACFOverrideSpeedNotifyState::StaticClass, TEXT("UACFOverrideSpeedNotifyState"), &Z_Registration_Info_UClass_UACFOverrideSpeedNotifyState, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UACFOverrideSpeedNotifyState), 575106419U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Animation_ACFOverrideSpeedNotifyState_h_2969102668(TEXT("/Script/AscentCombatFramework"),
		Z_CompiledInDeferFile_FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Animation_ACFOverrideSpeedNotifyState_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCombatFramework_Public_Animation_ACFOverrideSpeedNotifyState_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
