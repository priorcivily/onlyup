// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFActionsSetFactory.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASCENTEDITOREXTENSIONS_ACFActionsSetFactory_generated_h
#error "ACFActionsSetFactory.generated.h already included, missing '#pragma once' in ACFActionsSetFactory.h"
#endif
#define ASCENTEDITOREXTENSIONS_ACFActionsSetFactory_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_RPC_WRAPPERS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUACFActionsSetFactory(); \
	friend struct Z_Construct_UClass_UACFActionsSetFactory_Statics; \
public: \
	DECLARE_CLASS(UACFActionsSetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AscentEditorExtensions"), NO_API) \
	DECLARE_SERIALIZER(UACFActionsSetFactory)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFActionsSetFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFActionsSetFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFActionsSetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFActionsSetFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFActionsSetFactory(UACFActionsSetFactory&&); \
	NO_API UACFActionsSetFactory(const UACFActionsSetFactory&); \
public: \
	NO_API virtual ~UACFActionsSetFactory();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_9_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_RPC_WRAPPERS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_INCLASS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASCENTEDITOREXTENSIONS_API UClass* StaticClass<class UACFActionsSetFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentEditorExtensions_Private_ACFActionsSetFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
