// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ALSSaveTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASCENTSAVESYSTEM_ALSSaveTypes_generated_h
#error "ALSSaveTypes.generated.h already included, missing '#pragma once' in ALSSaveTypes.h"
#endif
#define ASCENTSAVESYSTEM_ALSSaveTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSBaseData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct();


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSBaseData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_63_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSObjectData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FALSBaseData Super;


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSObjectData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_118_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSComponentData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FALSObjectData Super;


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSComponentData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_155_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSActorData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FALSObjectData Super;


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSActorData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_228_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSLevelData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FALSObjectData Super;


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSLevelData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_263_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSActorLoaded_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct();


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSActorLoaded>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_277_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FALSPlayerData_Statics; \
	ASCENTSAVESYSTEM_API static class UScriptStruct* StaticStruct();


template<> ASCENTSAVESYSTEM_API UScriptStruct* StaticStruct<struct FALSPlayerData>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUALSSaveTypes(); \
	friend struct Z_Construct_UClass_UALSSaveTypes_Statics; \
public: \
	DECLARE_CLASS(UALSSaveTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AscentSaveSystem"), NO_API) \
	DECLARE_SERIALIZER(UALSSaveTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UALSSaveTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UALSSaveTypes(UALSSaveTypes&&); \
	NO_API UALSSaveTypes(const UALSSaveTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UALSSaveTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UALSSaveTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UALSSaveTypes) \
	NO_API virtual ~UALSSaveTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_296_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h_299_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASCENTSAVESYSTEM_API UClass* StaticClass<class UALSSaveTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentSaveSystem_Public_ALSSaveTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
