// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "CASTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGameplayTag;
#ifdef COMBINEDANIMATIONSSYSTEM_CASTypes_generated_h
#error "CASTypes.generated.h already included, missing '#pragma once' in CASTypes.h"
#endif
#define COMBINEDANIMATIONSSYSTEM_CASTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_14_DELEGATE \
COMBINEDANIMATIONSSYSTEM_API void FOnCombinedAnimationStarted_DelegateWrapper(const FMulticastScriptDelegate& OnCombinedAnimationStarted, FGameplayTag const& animTag);


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_15_DELEGATE \
COMBINEDANIMATIONSSYSTEM_API void FOnCombinedAnimationEnded_DelegateWrapper(const FMulticastScriptDelegate& OnCombinedAnimationEnded, FGameplayTag const& animTag);


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCombinedAnimsMaster_Statics; \
	COMBINEDANIMATIONSSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> COMBINEDANIMATIONSSYSTEM_API UScriptStruct* StaticStruct<struct FCombinedAnimsMaster>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_73_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCombinedAnimsSlave_Statics; \
	COMBINEDANIMATIONSSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> COMBINEDANIMATIONSSYSTEM_API UScriptStruct* StaticStruct<struct FCombinedAnimsSlave>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_93_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCurrentCombinedAnim_Statics; \
	COMBINEDANIMATIONSSYSTEM_API static class UScriptStruct* StaticStruct();


template<> COMBINEDANIMATIONSSYSTEM_API UScriptStruct* StaticStruct<struct FCurrentCombinedAnim>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCASTypes(); \
	friend struct Z_Construct_UClass_UCASTypes_Statics; \
public: \
	DECLARE_CLASS(UCASTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CombinedAnimationsSystem"), NO_API) \
	DECLARE_SERIALIZER(UCASTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCASTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCASTypes(UCASTypes&&); \
	NO_API UCASTypes(const UCASTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCASTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCASTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCASTypes) \
	NO_API virtual ~UCASTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_119_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h_121_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMBINEDANIMATIONSSYSTEM_API UClass* StaticClass<class UCASTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CombinedAnimationsSystem_Public_CASTypes_h


#define FOREACH_ENUM_ERELATIVEDIRECTION(op) \
	op(ERelativeDirection::EAny) \
	op(ERelativeDirection::EFrontal) \
	op(ERelativeDirection::EOpposite) 

enum class ERelativeDirection : uint8;
template<> struct TIsUEnumClass<ERelativeDirection> { enum { Value = true }; };
template<> COMBINEDANIMATIONSSYSTEM_API UEnum* StaticEnum<ERelativeDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
