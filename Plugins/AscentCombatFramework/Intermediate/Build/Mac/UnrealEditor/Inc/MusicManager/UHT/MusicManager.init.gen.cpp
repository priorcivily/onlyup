// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMusicManager_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_MusicManager;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_MusicManager()
	{
		if (!Z_Registration_Info_UPackage__Script_MusicManager.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/MusicManager",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xE379DB33,
				0x2BCA0C93,
				METADATA_PARAMS(0, nullptr)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_MusicManager.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_MusicManager.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_MusicManager(Z_Construct_UPackage__Script_MusicManager, TEXT("/Script/MusicManager"), Z_Registration_Info_UPackage__Script_MusicManager, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xE379DB33, 0x2BCA0C93));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
