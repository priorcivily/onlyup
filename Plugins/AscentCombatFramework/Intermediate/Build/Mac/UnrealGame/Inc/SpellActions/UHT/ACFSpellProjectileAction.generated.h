// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFSpellProjectileAction.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPELLACTIONS_ACFSpellProjectileAction_generated_h
#error "ACFSpellProjectileAction.generated.h already included, missing '#pragma once' in ACFSpellProjectileAction.h"
#endif
#define SPELLACTIONS_ACFSpellProjectileAction_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFSpellProjectileAction(); \
	friend struct Z_Construct_UClass_UACFSpellProjectileAction_Statics; \
public: \
	DECLARE_CLASS(UACFSpellProjectileAction, UACFBaseAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpellActions"), NO_API) \
	DECLARE_SERIALIZER(UACFSpellProjectileAction)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFSpellProjectileAction(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFSpellProjectileAction(UACFSpellProjectileAction&&); \
	NO_API UACFSpellProjectileAction(const UACFSpellProjectileAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFSpellProjectileAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFSpellProjectileAction); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UACFSpellProjectileAction) \
	NO_API virtual ~UACFSpellProjectileAction();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_12_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPELLACTIONS_API UClass* StaticClass<class UACFSpellProjectileAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFSpellProjectileAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
