// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFBuffAction.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPELLACTIONS_ACFBuffAction_generated_h
#error "ACFBuffAction.generated.h already included, missing '#pragma once' in ACFBuffAction.h"
#endif
#define SPELLACTIONS_ACFBuffAction_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFBuffAction(); \
	friend struct Z_Construct_UClass_UACFBuffAction_Statics; \
public: \
	DECLARE_CLASS(UACFBuffAction, UACFBaseAction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpellActions"), NO_API) \
	DECLARE_SERIALIZER(UACFBuffAction)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFBuffAction(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFBuffAction(UACFBuffAction&&); \
	NO_API UACFBuffAction(const UACFBuffAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFBuffAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFBuffAction); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UACFBuffAction) \
	NO_API virtual ~UACFBuffAction();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_13_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPELLACTIONS_API UClass* StaticClass<class UACFBuffAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_SpellActions_Public_ACFBuffAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
