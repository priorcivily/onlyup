// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ARSTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ADVANCEDRPGSYSTEM_ARSTypes_generated_h
#error "ARSTypes.generated.h already included, missing '#pragma once' in ARSTypes.h"
#endif
#define ADVANCEDRPGSYSTEM_ARSTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_46_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseModifier_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FBaseModifier>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttributeModifier_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseModifier Super;


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttributeModifier>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_92_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStatisticsModifier_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseModifier Super;


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FStatisticsModifier>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_121_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStatistic_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FStatistic>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_239_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttribute_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttribute>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_313_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttributesSet_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttributesSet>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_332_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttributesSetModifier_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttributesSetModifier>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_359_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStatisticValue_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FStatisticValue>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_382_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttributeInfluence_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttributeInfluence>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_396_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FStatInfluence_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FStatInfluence>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_417_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGenerationRule_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FGenerationRule>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_442_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttributesByLevel_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FAttributesByLevel>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_471_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTimedAttributeSetModifier_Statics; \
	ADVANCEDRPGSYSTEM_API static class UScriptStruct* StaticStruct();


template<> ADVANCEDRPGSYSTEM_API UScriptStruct* StaticStruct<struct FTimedAttributeSetModifier>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUARSTypes(); \
	friend struct Z_Construct_UClass_UARSTypes_Statics; \
public: \
	DECLARE_CLASS(UARSTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AdvancedRPGSystem"), NO_API) \
	DECLARE_SERIALIZER(UARSTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UARSTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UARSTypes(UARSTypes&&); \
	NO_API UARSTypes(const UARSTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UARSTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UARSTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UARSTypes) \
	NO_API virtual ~UARSTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_485_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h_487_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ADVANCEDRPGSYSTEM_API UClass* StaticClass<class UARSTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AdvancedRPGSystem_Public_ARSTypes_h


#define FOREACH_ENUM_EMODIFIERTYPE(op) \
	op(EModifierType::EAdditive) \
	op(EModifierType::EPercentage) 

enum class EModifierType : uint8;
template<> struct TIsUEnumClass<EModifierType> { enum { Value = true }; };
template<> ADVANCEDRPGSYSTEM_API UEnum* StaticEnum<EModifierType>();

#define FOREACH_ENUM_ESTATSLOADMETHOD(op) \
	op(EStatsLoadMethod::EUseDefaultsWithoutGeneration) \
	op(EStatsLoadMethod::EGenerateFromDefaultsPrimary) \
	op(EStatsLoadMethod::ELoadByLevel) 

enum class EStatsLoadMethod : uint8;
template<> struct TIsUEnumClass<EStatsLoadMethod> { enum { Value = true }; };
template<> ADVANCEDRPGSYSTEM_API UEnum* StaticEnum<EStatsLoadMethod>();

#define FOREACH_ENUM_ELEVELINGTYPE(op) \
	op(ELevelingType::ECantLevelUp) \
	op(ELevelingType::EGenerateNewStatsFromCurves) \
	op(ELevelingType::EAssignPerksManually) 

enum class ELevelingType : uint8;
template<> struct TIsUEnumClass<ELevelingType> { enum { Value = true }; };
template<> ADVANCEDRPGSYSTEM_API UEnum* StaticEnum<ELevelingType>();

#define FOREACH_ENUM_ESTATISTICSTYPE(op) \
	op(EStatisticsType::EStatistic) \
	op(EStatisticsType::EPrimaryAttribute) \
	op(EStatisticsType::ESecondaryAttribute) 

enum class EStatisticsType : uint8;
template<> struct TIsUEnumClass<EStatisticsType> { enum { Value = true }; };
template<> ADVANCEDRPGSYSTEM_API UEnum* StaticEnum<EStatisticsType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
