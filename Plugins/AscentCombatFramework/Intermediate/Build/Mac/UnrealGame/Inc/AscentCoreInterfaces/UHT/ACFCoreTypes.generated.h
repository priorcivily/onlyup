// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFCoreTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASCENTCOREINTERFACES_ACFCoreTypes_generated_h
#error "ACFCoreTypes.generated.h already included, missing '#pragma once' in ACFCoreTypes.h"
#endif
#define ASCENTCOREINTERFACES_ACFCoreTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTeamInfo_Statics; \
	ASCENTCOREINTERFACES_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOREINTERFACES_API UScriptStruct* StaticStruct<struct FTeamInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFStruct_Statics; \
	ASCENTCOREINTERFACES_API static class UScriptStruct* StaticStruct();


template<> ASCENTCOREINTERFACES_API UScriptStruct* StaticStruct<struct FACFStruct>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFCoreTypes(); \
	friend struct Z_Construct_UClass_UACFCoreTypes_Statics; \
public: \
	DECLARE_CLASS(UACFCoreTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AscentCoreInterfaces"), NO_API) \
	DECLARE_SERIALIZER(UACFCoreTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFCoreTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFCoreTypes(UACFCoreTypes&&); \
	NO_API UACFCoreTypes(const UACFCoreTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFCoreTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFCoreTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFCoreTypes) \
	NO_API virtual ~UACFCoreTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_93_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h_95_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASCENTCOREINTERFACES_API UClass* StaticClass<class UACFCoreTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AscentCoreInterfaces_Public_ACFCoreTypes_h


#define FOREACH_ENUM_ETEAM(op) \
	op(ETeam::ETeam1) \
	op(ETeam::ETeam2) \
	op(ETeam::ETeam3) \
	op(ETeam::ETeam4) \
	op(ETeam::ENeutral) 

enum class ETeam : uint8;
template<> struct TIsUEnumClass<ETeam> { enum { Value = true }; };
template<> ASCENTCOREINTERFACES_API UEnum* StaticEnum<ETeam>();

#define FOREACH_ENUM_EBATTLETYPE(op) \
	op(EBattleType::ETeamBased) \
	op(EBattleType::EEveryoneAgainstEveryone) 

enum class EBattleType : uint8;
template<> struct TIsUEnumClass<EBattleType> { enum { Value = true }; };
template<> ASCENTCOREINTERFACES_API UEnum* StaticEnum<EBattleType>();

#define FOREACH_ENUM_EACFDIRECTION(op) \
	op(EACFDirection::Front) \
	op(EACFDirection::Back) \
	op(EACFDirection::Left) \
	op(EACFDirection::Right) 

enum class EACFDirection : uint8;
template<> struct TIsUEnumClass<EACFDirection> { enum { Value = true }; };
template<> ASCENTCOREINTERFACES_API UEnum* StaticEnum<EACFDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
