// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Animation/ACFAnimTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CHARACTERCONTROLLER_ACFAnimTypes_generated_h
#error "ACFAnimTypes.generated.h already included, missing '#pragma once' in ACFAnimTypes.h"
#endif
#define CHARACTERCONTROLLER_ACFAnimTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_Animation_ACFAnimTypes_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoveset_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FMoveset>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_Animation_ACFAnimTypes_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOverlayLayer_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FOverlayLayer>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_Animation_ACFAnimTypes_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRiderLayer_Statics; \
	CHARACTERCONTROLLER_API static class UScriptStruct* StaticStruct(); \
	typedef FACFStruct Super;


template<> CHARACTERCONTROLLER_API UScriptStruct* StaticStruct<struct FRiderLayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CharacterController_Public_Animation_ACFAnimTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
