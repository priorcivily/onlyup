// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFItemTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef INVENTORYSYSTEM_ACFItemTypes_generated_h
#error "ACFItemTypes.generated.h already included, missing '#pragma once' in ACFItemTypes.h"
#endif
#define INVENTORYSYSTEM_ACFItemTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FModularPart_Statics; \
	INVENTORYSYSTEM_API static class UScriptStruct* StaticStruct();


template<> INVENTORYSYSTEM_API UScriptStruct* StaticStruct<struct FModularPart>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_94_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FItemGenerationSlot_Statics; \
	INVENTORYSYSTEM_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> INVENTORYSYSTEM_API UScriptStruct* StaticStruct<struct FItemGenerationSlot>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_114_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FACFItemGenerationRule_Statics; \
	INVENTORYSYSTEM_API static class UScriptStruct* StaticStruct();


template<> INVENTORYSYSTEM_API UScriptStruct* StaticStruct<struct FACFItemGenerationRule>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFItemTypes(); \
	friend struct Z_Construct_UClass_UACFItemTypes_Statics; \
public: \
	DECLARE_CLASS(UACFItemTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/InventorySystem"), NO_API) \
	DECLARE_SERIALIZER(UACFItemTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFItemTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFItemTypes(UACFItemTypes&&); \
	NO_API UACFItemTypes(const UACFItemTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFItemTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFItemTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFItemTypes) \
	NO_API virtual ~UACFItemTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_143_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h_145_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> INVENTORYSYSTEM_API UClass* StaticClass<class UACFItemTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_InventorySystem_Public_ACFItemTypes_h


#define FOREACH_ENUM_EPROJECTILEHITPOLICY(op) \
	op(EProjectileHitPolicy::DestroyOnHit) \
	op(EProjectileHitPolicy::AttachOnHit) 

enum class EProjectileHitPolicy : uint8;
template<> struct TIsUEnumClass<EProjectileHitPolicy> { enum { Value = true }; };
template<> INVENTORYSYSTEM_API UEnum* StaticEnum<EProjectileHitPolicy>();

#define FOREACH_ENUM_EHANDLETYPE(op) \
	op(EHandleType::OneHanded) \
	op(EHandleType::OffHand) \
	op(EHandleType::TwoHanded) 

enum class EHandleType : uint8;
template<> struct TIsUEnumClass<EHandleType> { enum { Value = true }; };
template<> INVENTORYSYSTEM_API UEnum* StaticEnum<EHandleType>();

#define FOREACH_ENUM_ESHOOTTARGETTYPE(op) \
	op(EShootTargetType::CameraTowardsFocus) \
	op(EShootTargetType::PawnForward) \
	op(EShootTargetType::PawnTowardsFocus) \
	op(EShootTargetType::WeaponForward) \
	op(EShootTargetType::WeaponTowardsFocus) 

enum class EShootTargetType : uint8;
template<> struct TIsUEnumClass<EShootTargetType> { enum { Value = true }; };
template<> INVENTORYSYSTEM_API UEnum* StaticEnum<EShootTargetType>();

#define FOREACH_ENUM_EITEMTYPE(op) \
	op(EItemType::Armor) \
	op(EItemType::MeleeWeapon) \
	op(EItemType::RangedWeapon) \
	op(EItemType::Consumable) \
	op(EItemType::Material) \
	op(EItemType::Accessory) \
	op(EItemType::Projectile) \
	op(EItemType::Other) 

enum class EItemType : uint8;
template<> struct TIsUEnumClass<EItemType> { enum { Value = true }; };
template<> INVENTORYSYSTEM_API UEnum* StaticEnum<EItemType>();

#define FOREACH_ENUM_ESHOOTINGTYPE(op) \
	op(EShootingType::EProjectile) \
	op(EShootingType::ESwipeTrace) 

enum class EShootingType : uint8;
template<> struct TIsUEnumClass<EShootingType> { enum { Value = true }; };
template<> INVENTORYSYSTEM_API UEnum* StaticEnum<EShootingType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
