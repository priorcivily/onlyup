// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACFAITypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIFRAMEWORK_ACFAITypes_generated_h
#error "ACFAITypes.generated.h already included, missing '#pragma once' in ACFAITypes.h"
#endif
#define AIFRAMEWORK_ACFAITypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPatrolConfig_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FPatrolConfig>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_63_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAIAgentsInfo_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FAIAgentsInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_110_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommandChances_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FCommandChances>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_127_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAICombatStateConfig_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FAICombatStateConfig>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_189_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConditions_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FActionChances Super;


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FConditions>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_203_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseUnit_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FBaseUnit>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_244_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAISpawnInfo_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseUnit Super;


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FAISpawnInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_281_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWaveConfig_Statics; \
	AIFRAMEWORK_API static class UScriptStruct* StaticStruct();


template<> AIFRAMEWORK_API UScriptStruct* StaticStruct<struct FWaveConfig>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACFAITypes(); \
	friend struct Z_Construct_UClass_UACFAITypes_Statics; \
public: \
	DECLARE_CLASS(UACFAITypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AIFramework"), NO_API) \
	DECLARE_SERIALIZER(UACFAITypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACFAITypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACFAITypes(UACFAITypes&&); \
	NO_API UACFAITypes(const UACFAITypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACFAITypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACFAITypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACFAITypes) \
	NO_API virtual ~UACFAITypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_320_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h_322_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AIFRAMEWORK_API UClass* StaticClass<class UACFAITypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_AIFramework_Public_ACFAITypes_h


#define FOREACH_ENUM_EAGENTROTATION(op) \
	op(EAgentRotation::EGroupForward) \
	op(EAgentRotation::EGroupCenter) 

enum class EAgentRotation : uint8;
template<> struct TIsUEnumClass<EAgentRotation> { enum { Value = true }; };
template<> AIFRAMEWORK_API UEnum* StaticEnum<EAgentRotation>();

#define FOREACH_ENUM_EAISTATE(op) \
	op(EAIState::EPatrol) \
	op(EAIState::EBattle) \
	op(EAIState::EFollowLeader) \
	op(EAIState::EReturnHome) \
	op(EAIState::EWait) 

enum class EAIState : uint8;
template<> struct TIsUEnumClass<EAIState> { enum { Value = true }; };
template<> AIFRAMEWORK_API UEnum* StaticEnum<EAIState>();

#define FOREACH_ENUM_EAICOMBATSTATE(op) \
	op(EAICombatState::EMeleeCombat) \
	op(EAICombatState::EChaseTarget) \
	op(EAICombatState::ERangedCombat) \
	op(EAICombatState::EStudyTarget) \
	op(EAICombatState::EFlee) 

enum class EAICombatState : uint8;
template<> struct TIsUEnumClass<EAICombatState> { enum { Value = true }; };
template<> AIFRAMEWORK_API UEnum* StaticEnum<EAICombatState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
