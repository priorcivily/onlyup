// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ACMTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COLLISIONSMANAGER_ACMTypes_generated_h
#error "ACMTypes.generated.h already included, missing '#pragma once' in ACMTypes.h"
#endif
#define COLLISIONSMANAGER_ACMTypes_generated_h

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAreaDamageInfo_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct();


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FAreaDamageInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_46_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FHitActors_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct();


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FHitActors>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_75_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseFX_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FBaseFX>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_104_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FActionEffect_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseFX Super;


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FActionEffect>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_140_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImpactFX_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseFX Super;


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FImpactFX>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_181_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMaterialImpactFX_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseFX Super;


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FMaterialImpactFX>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_215_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImpactsArray_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct();


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FImpactsArray>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_228_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBaseTraceInfo_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct();


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FBaseTraceInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_251_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTraceInfo_Statics; \
	COLLISIONSMANAGER_API static class UScriptStruct* StaticStruct(); \
	typedef FBaseTraceInfo Super;


template<> COLLISIONSMANAGER_API UScriptStruct* StaticStruct<struct FTraceInfo>();

#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_SPARSE_DATA
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_ACCESSORS
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUACMTypes(); \
	friend struct Z_Construct_UClass_UACMTypes_Statics; \
public: \
	DECLARE_CLASS(UACMTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CollisionsManager"), NO_API) \
	DECLARE_SERIALIZER(UACMTypes)


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UACMTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UACMTypes(UACMTypes&&); \
	NO_API UACMTypes(const UACMTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UACMTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UACMTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UACMTypes) \
	NO_API virtual ~UACMTypes();


#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_293_PROLOG
#define FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_SPARSE_DATA \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_ACCESSORS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_INCLASS_NO_PURE_DECLS \
	FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h_295_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COLLISIONSMANAGER_API UClass* StaticClass<class UACMTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Build_U5M_Marketplace_Mac_Sync_LocalBuilds_PluginTemp_HostProject_Plugins_AscentCombatFramework_Source_CollisionsManager_Public_ACMTypes_h


#define FOREACH_ENUM_EDEBUGTYPE(op) \
	op(EDebugType::EDontShowDebugInfos) \
	op(EDebugType::EShowInfoDuringSwing) \
	op(EDebugType::EAlwaysShowDebug) 

enum class EDebugType : uint8;
template<> struct TIsUEnumClass<EDebugType> { enum { Value = true }; };
template<> COLLISIONSMANAGER_API UEnum* StaticEnum<EDebugType>();

#define FOREACH_ENUM_EDAMAGETYPE(op) \
	op(EDamageType::EPoint) \
	op(EDamageType::EArea) 

enum class EDamageType : uint8;
template<> struct TIsUEnumClass<EDamageType> { enum { Value = true }; };
template<> COLLISIONSMANAGER_API UEnum* StaticEnum<EDamageType>();

#define FOREACH_ENUM_ESPAWNFXLOCATION(op) \
	op(ESpawnFXLocation::ESpawnOnActorLocation) \
	op(ESpawnFXLocation::ESpawnAttachedToSocketOrBone) \
	op(ESpawnFXLocation::ESpawnAtLocation) 

enum class ESpawnFXLocation : uint8;
template<> struct TIsUEnumClass<ESpawnFXLocation> { enum { Value = true }; };
template<> COLLISIONSMANAGER_API UEnum* StaticEnum<ESpawnFXLocation>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
